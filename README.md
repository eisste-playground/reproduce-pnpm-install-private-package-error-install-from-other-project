# Reproduce installation error when using pnpm to install a private GitLab registry package

## Steps to reproduce

1. Open the projects' root folder in your terminal
2. Execute this to add your personal access token `pnpm config set -- //gitlab.com/api/v4/packages/npm/:_authToken=YOUR_PERSONAL_ACCESS_TOKEN`
3. Execute this to add the package registry `pnpm config set @eisste-playground:registry https://gitlab.com/api/v4/packages/npm/`
4. Try to install the package in this project like that: `pnpm i` (dependency "@eisste-playground/reproduce-pnpm-install-private-package-error" is already listed in package.json)
5. Get the error messages `ERR_PNPM_FETCH_404  GET https://gitlab.com/api/v4/projects/41694919/packages/npm/@eisste-playground/reproduce-pnpm-install-private-package-error/-/@eisste-playground/reproduce-pnpm-install-private-package-error-1.0.0.tgz: Not Found - 404` and `No authorization header was set for the request.`.
6. Remove node_modules `rm -r node_modules`
7. Repeat steps 1-4 above switching `pnpm` with `npm` and see that the installation is successful now.

